package kr.ac.pusan.bsclab.bab.special.pmfcc.web.services;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

@Service
public class FactoryServiceImpl implements FactoryService{

	@Autowired
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void addFactory(String factoryCode, String factoryName) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update("INSERT INTO factory(factory_code,factory_name)VALUES(?,?)",factoryCode, factoryName);
	}

	@Override
	public void deleteFactory(long factoryId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update("DELETE FROM factory WHERE factory_id = '"+factoryId+"'");
	}

}
