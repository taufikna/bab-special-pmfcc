package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pmfcc/facility_rest")
public class FacilityRestController {

	@RequestMapping(value = "get_factories")
	public ArrayList<Factory> getFactories() {

		ArrayList<Factory> f = new ArrayList<Factory>();
		
		Factory f1 = new Factory(); 
		Factory f2 = new Factory(); 
		
		f1.id="1"; f1.name="(F001) Factory 1";		
		f2.id="2"; f2.name="(F002) Factory 2";
		
		f.add(f1);
		f.add(f2);
		
		return f;
	}
	
	@RequestMapping(value = "get_machines")
	public ArrayList<Machine> getMachines(@RequestBody Factory fact) {

		ArrayList<Machine> f = new ArrayList<Machine>();
		
		Machine m1 = new Machine();
		Machine m2 = new Machine();
		Machine m3 = new Machine();
		
		m1.id="MC_0001"; m1.name="MC_0001";
		m2.id="MC_0002"; m2.name="MC_0002";
		m3.id="MC_0003"; m3.name="MC_0003";
		
		f.add(m1);
		f.add(m2);
		f.add(m3);
		
		return f;
	}
	
	@RequestMapping(value = "get_results")
	public HashMap<String, ArrayList<?>> getResults(@RequestBody Results fact) {
		
		HashMap<String, ArrayList<?>> m = new HashMap<String, ArrayList<?>>();
		ArrayList<String> a = new ArrayList<String>();
		for(int i=0;i<33;i++) {
			a.add(Integer.toString(i));
		}
		m.put("column_idx", a);
		
		ArrayList<ArrayList<String>> a2 = new ArrayList<ArrayList<String>>();
		ArrayList<String> b = new ArrayList<String>();
		b.add("2018/09/10 01:00:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b2 = new ArrayList<String>();
		b2.add("2018/09/10 01:10:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b2.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b3 = new ArrayList<String>();
		b3.add("2018/09/10 01:20:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b3.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b4 = new ArrayList<String>();
		b4.add("2018/09/10 01:30:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b4.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b5 = new ArrayList<String>();
		b5.add("2018/09/10 01:40:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b5.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b6 = new ArrayList<String>();
		b6.add("2018/09/10 01:50:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b6.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b7 = new ArrayList<String>();
		b7.add("2018/09/10 02:00:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b7.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b8 = new ArrayList<String>();
		b8.add("2018/09/10 02:10:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b8.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b9 = new ArrayList<String>();
		b9.add("2018/09/10 02:20:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b9.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b10 = new ArrayList<String>();
		b10.add("2018/09/10 02:30:00");
		for(int i=0;i<33;i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 200 + 1);
			b10.add(Integer.toString(randomNum));
		}
		
		a2.add(b);
		a2.add(b2);
		a2.add(b3);
		a2.add(b4);
		a2.add(b5);
		a2.add(b6);
		a2.add(b7);
		a2.add(b8);
		a2.add(b9);
		a2.add(b10);
		m.put("data", a2);
		
		ArrayList<String> a4 = new ArrayList<String>();
		a4.add("2018/09/10 02:30:00");
		m.put("last_time", a4);
		
		ArrayList<String> a3 = new ArrayList<String>();
		a3.add("Date");
		a3.add("Comm check");
		a3.add("Setting Status");
		a3.add("Current Status");
		a3.add("Total Power(current)");
		a3.add("Delata input Voltage");
		a3.add("Star input voltage");
		a3.add("Delta input current");
		a3.add("Star input current");
		a3.add("DC Voltage");
		a3.add("Sequence number");
		a3.add("Fault Number(current)");
		a3.add("Inverter No.1 Power Command (SV)");
		a3.add("Inverter No.2 Power Command (SV)");
		a3.add("Converter Cooling water temp");
		a3.add("Inverter no.1 cooling water temp");
		a3.add("Furnace No.1 cooling water temp");
		a3.add("Inverter no.2 cooling water temp");
		a3.add("Furnace No.2 cooling water temp");
		a3.add("Leak Level(100%)");
		a3.add("Comm check");
		a3.add("Inverter internal Status");
		a3.add("Inverter external Status");
		a3.add("Current Power(current)");
		a3.add("Frequency");
		a3.add("DC Voltage");
		a3.add("DC Current");
		a3.add("Output Voltage");
		a3.add("Output Current");
		a3.add("Fault Number");
		a3.add("Sequence Number");
		a3.add("Last Heating Time");
		a3.add("Q Factor");
		a3.add("Leak Level(1000%)");
		
		m.put("label", a3);
		
		return m;
	}
}
