package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import org.springframework.stereotype.Controller;

@Controller
public abstract class AWebController {
	public static final String HOST = "http://localhost:8080";
	public static final String URL_BASE = "/pmfcc";
}
