package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers.AWebController;
import kr.ac.pusan.bsclab.bab.special.pmfcc.web.views.View;

import kr.ac.pusan.bsclab.bab.special.pmfcc.web.models.Factory;
import kr.ac.pusan.bsclab.bab.special.pmfcc.web.daos.FactoryRepository;
import kr.ac.pusan.bsclab.bab.special.pmfcc.web.services.FactoryService;

@Controller
public class FactoryController {
	public static final String HOST = AWebController.HOST;
	public static final String URL_BASE = AWebController.URL_BASE+"/factory";
	public static final String VIEW_BASE = "/master";
	
	@Autowired
	private FactoryRepository factoryRepository;
	
	@Autowired
	private FactoryService factoryService;
	
	
	@RequestMapping(method = RequestMethod.GET, path=URL_BASE)
	public @ResponseBody View getIndex(Model model) {
		View view = new View(VIEW_BASE+"/factory");
		
		model.addAttribute("factories", factoryRepository.findAll());
		
		view.addObject("host",HOST);
		view.addObject("urlBase",AWebController.URL_BASE);
		return view;
	}
	
	@RequestMapping(method = RequestMethod.POST, path=URL_BASE)
	public @ResponseBody View addFactory(Model model, @RequestParam("factoryCode") String factoryCode, @RequestParam("factoryName") String factoryName) {
		View view = new View(VIEW_BASE+"/factory");
		
		factoryService.addFactory(factoryCode, factoryName);
		System.out.println("Factory Code"+ factoryCode +", Factory Name"+ factoryName);
		
		model.addAttribute("factories", factoryRepository.findAll());
		
		view.addObject("host",HOST);
		view.addObject("urlBase",AWebController.URL_BASE);
		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, path=URL_BASE+"/delete")
	public String deleteFactory(Model model, @RequestParam("factoryId") String factoryId) {				
		factoryService.deleteFactory(Long.parseLong(factoryId));
		System.out.println("Deleting: "+factoryId);
		
		model.addAttribute("factories", factoryRepository.findAll());
			
		return "redirect:"+URL_BASE;
	}
}
