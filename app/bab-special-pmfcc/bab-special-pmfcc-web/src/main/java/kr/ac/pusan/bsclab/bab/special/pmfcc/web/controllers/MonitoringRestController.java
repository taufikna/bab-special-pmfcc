package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pmfcc/monitoring_rest")
public class MonitoringRestController {

	@RequestMapping(value = "get_factories")
	public ArrayList<Factory> getFactories() {

		ArrayList<Factory> f = new ArrayList<Factory>();
		
		Factory f1 = new Factory(); 
		Factory f2 = new Factory(); 
		
		f1.id="1"; f1.name="(F001) Factory 1";		
		f2.id="2"; f2.name="(F002) Factory 2";
		
		f.add(f1);
		f.add(f2);
		
		return f;
	}
	
	@RequestMapping(value = "get_machines")
	public ArrayList<Machine> getMachines(@RequestBody Factory fact) {

		ArrayList<Machine> f = new ArrayList<Machine>();
		
		Machine m1 = new Machine();
		Machine m2 = new Machine();
		Machine m3 = new Machine();
		
		m1.id="MC_0001"; m1.name="MC_0001";
		m2.id="MC_0002"; m2.name="MC_0002";
		m3.id="MC_0003"; m3.name="MC_0003";
		
		f.add(m1);
		f.add(m2);
		f.add(m3);
		
		return f;
	}
	
	@RequestMapping(value = "get_results_by_hour")
	public HashMap<String, ArrayList<?>> getResultsByHour(@RequestBody ResultsByHour fact) {
		
		int cnt = Integer.parseInt(fact.cnt);	
		cnt = cnt % 30;
		Random randnum = new Random(123);
		for(int i=0;i<33;i++) {
			for(int j=0;j<cnt;j++) {
				randnum.nextInt();
			}
		}
		
		HashMap<String, ArrayList<?>> m = new HashMap<String, ArrayList<?>>();
		ArrayList<String> a = new ArrayList<String>();
		for(int i=0;i<33;i++) {
			a.add(Integer.toString(i));
		}
		m.put("column_idx", a);
		
		ArrayList<ArrayList<String>> a2 = new ArrayList<ArrayList<String>>();
		ArrayList<String> b = new ArrayList<String>();
		b.add("2018/09/10 01:00:"+String.format("%02d", cnt));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b2 = new ArrayList<String>();
		b2.add("2018/09/10 01:00:"+String.format("%02d", cnt+1));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b2.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b3 = new ArrayList<String>();
		b3.add("2018/09/10 01:00:"+String.format("%02d", cnt+2));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b3.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b4 = new ArrayList<String>();
		b4.add("2018/09/10 01:00:"+String.format("%02d", cnt+3));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b4.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b5 = new ArrayList<String>();
		b5.add("2018/09/10 01:00:"+String.format("%02d", cnt+4));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b5.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b6 = new ArrayList<String>();
		b6.add("2018/09/10 01:00:"+String.format("%02d", cnt+5));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b6.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b7 = new ArrayList<String>();
		b7.add("2018/09/10 01:00:"+String.format("%02d", cnt+6));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b7.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b8 = new ArrayList<String>();
		b8.add("2018/09/10 01:00:"+String.format("%02d", cnt+7));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b8.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b9 = new ArrayList<String>();
		b9.add("2018/09/10 01:00:"+String.format("%02d", cnt+8));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b9.add(Integer.toString(randomNum));
		}
		
		ArrayList<String> b10 = new ArrayList<String>();
		b10.add("2018/09/10 01:00:"+String.format("%02d", cnt+9));
		for(int i=0;i<33;i++) {
			int randomNum = randnum.nextInt(200);
			b10.add(Integer.toString(randomNum));
		}
		
		a2.add(b);
		a2.add(b2);
		a2.add(b3);
		a2.add(b4);
		a2.add(b5);
		a2.add(b6);
		a2.add(b7);
		a2.add(b8);
		a2.add(b9);
		a2.add(b10);
		m.put("data", a2);
		
		ArrayList<String> a4 = new ArrayList<String>();
		a4.add("2018/09/10 02:30:00");
		m.put("last_time", a4);
		
		ArrayList<String> a3 = new ArrayList<String>();
		a3.add("Date");
		a3.add("Comm check");
		a3.add("Setting Status");
		a3.add("Current Status");
		a3.add("Total Power(current)");
		a3.add("Delata input Voltage");
		a3.add("Star input voltage");
		a3.add("Delta input current");
		a3.add("Star input current");
		a3.add("DC Voltage");
		a3.add("Sequence number");
		a3.add("Fault Number(current)");
		a3.add("Inverter No.1 Power Command (SV)");
		a3.add("Inverter No.2 Power Command (SV)");
		a3.add("Converter Cooling water temp");
		a3.add("Inverter no.1 cooling water temp");
		a3.add("Furnace No.1 cooling water temp");
		a3.add("Inverter no.2 cooling water temp");
		a3.add("Furnace No.2 cooling water temp");
		a3.add("Leak Level(100%)");
		a3.add("Comm check");
		a3.add("Inverter internal Status");
		a3.add("Inverter external Status");
		a3.add("Current Power(current)");
		a3.add("Frequency");
		a3.add("DC Voltage");
		a3.add("DC Current");
		a3.add("Output Voltage");
		a3.add("Output Current");
		a3.add("Fault Number");
		a3.add("Sequence Number");
		a3.add("Last Heating Time");
		a3.add("Q Factor");
		a3.add("Leak Level(1000%)");
		
		m.put("label", a3);
		
		return m;
	}
	
	@RequestMapping(value = "get_machine_parameters")
	public String getMachineParameters(@RequestBody Machine m) {
		
		return "<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D0\" class='checkparameter' checked> <label>Comm\r\n" + 
				"											Check</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D1\" class='checkparameter' checked> <label>Setting\r\n" + 
				"											Status</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D2\" class='checkparameter' checked> <label>Current\r\n" + 
				"											Status</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D3\" class='checkparameter' checked> <label>Total\r\n" + 
				"											Power(current)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D4\" class='checkparameter' checked> <label>Delta\r\n" + 
				"											Input Voltage</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D5\" class='checkparameter' checked> <label>Star\r\n" + 
				"											Input Voltage</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D6\" class='checkparameter' checked> <label>Delta\r\n" + 
				"											Input Current</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D7\" class='checkparameter' checked> <label>Star\r\n" + 
				"											Input Current</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D8\" class='checkparameter' checked> <label>DC\r\n" + 
				"											Voltage</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D9\" class='checkparameter' checked> <label>Sequence\r\n" + 
				"											Number</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D10\" class='checkparameter' checked> <label>Fault\r\n" + 
				"											Number(current)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D11\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter No. 1 Power Command (SV)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D12\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter No. 2 Power Command (SV)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D13\" class='checkparameter' checked>\r\n" + 
				"										<label>Converter Cooling Water Temperature</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D14\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter no. 1 Cooling Water Temperature</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D15\" class='checkparameter' checked>\r\n" + 
				"										<label>Furnace no. 1 Cooling Water Temperature</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D16\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter no. 2 Cooling Water Temperature</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D17\" class='checkparameter' checked>\r\n" + 
				"										<label>Furnace no. 2 Cooling Water Temperature</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D18\" class='checkparameter' checked> <label>Leak\r\n" + 
				"											Level (100%)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D19\" class='checkparameter' checked> <label>Comm\r\n" + 
				"											Check</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D20\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter Internal Status</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D21\" class='checkparameter' checked>\r\n" + 
				"										<label>Inverter External Status</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D22\" class='checkparameter' checked> <label>Current\r\n" + 
				"											Power (Current)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D23\" class='checkparameter' checked> <label>Frequency</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D24\" class='checkparameter' checked> <label>DC\r\n" + 
				"											Voltage</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D25\" class='checkparameter' checked> <label>DC\r\n" + 
				"											Current</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D26\" class='checkparameter' checked> <label>Output\r\n" + 
				"											Voltage</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D27\" class='checkparameter' checked> <label>Output\r\n" + 
				"											Current</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D28\" class='checkparameter' checked> <label>Fault\r\n" + 
				"											Number</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D29\" class='checkparameter' checked> <label>Sequence\r\n" + 
				"											Number</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D30\" class='checkparameter' checked> <label>Last\r\n" + 
				"											Heating Time</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D31\" class='checkparameter' checked> <label>Q\r\n" + 
				"											Factor</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>\r\n" + 
				"								<div class=\"field\">\r\n" + 
				"									<div class=\"ui checkbox\">\r\n" + 
				"										<input type=\"checkbox\" name=\"D32\" class='checkparameter' checked> <label>Leak\r\n" + 
				"											Level (1000%)</label>\r\n" + 
				"									</div>\r\n" + 
				"								</div>";
	}
}

class Factory {
	public String id;
	public String name;
}

class Machine {
	public String id;
	public String name;
}

class ResultsByHour {
	public String factory_id;
	public String machine_id;
	public String hour;
	public String cnt;
}

class Results {
	public String factory_id;
	public String machine_id;
	public String start_date_field;
	public String end_date_field;
}