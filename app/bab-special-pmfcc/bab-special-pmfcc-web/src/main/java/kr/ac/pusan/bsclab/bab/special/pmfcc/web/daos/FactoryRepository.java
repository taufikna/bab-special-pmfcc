package kr.ac.pusan.bsclab.bab.special.pmfcc.web.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.special.pmfcc.web.models.Factory;

@Repository
public interface FactoryRepository extends CrudRepository<Factory, Long>{

}
