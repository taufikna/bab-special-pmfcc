package kr.ac.pusan.bsclab.bab.special.pmfcc.web.views;

import org.springframework.web.servlet.ModelAndView;

public class View extends ModelAndView {
	public View(String viewName) {
		super(viewName);
	}
	
	public View(String viewName, Object model) {
		super(viewName);
		addObject("model", model);
	}
}
