package kr.ac.pusan.bsclab.bab.special.pmfcc.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabSpecialPmfccWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BabSpecialPmfccWebApplication.class, args);
	}
}
