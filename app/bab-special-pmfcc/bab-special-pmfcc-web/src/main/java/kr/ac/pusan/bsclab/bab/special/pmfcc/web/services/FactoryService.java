package kr.ac.pusan.bsclab.bab.special.pmfcc.web.services;

public interface FactoryService {
	void addFactory(String factoryCode, String factoryName);
	void deleteFactory(long factoryId);
}
