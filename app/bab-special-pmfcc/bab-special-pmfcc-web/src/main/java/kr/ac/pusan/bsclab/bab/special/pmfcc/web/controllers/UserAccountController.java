package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers.AWebController;
import kr.ac.pusan.bsclab.bab.special.pmfcc.web.views.View;

@Controller
public class UserAccountController {
	public static final String HOST = AWebController.HOST;
	public static final String URL_BASE = AWebController.URL_BASE+"/mduac";
	public static final String VIEW_BASE = "/master";
	
	@RequestMapping(method = RequestMethod.GET, path=URL_BASE)
	public @ResponseBody View getIndex() {
		View view = new View(VIEW_BASE+"/useraccount");
		
		view.addObject("host",HOST);
		view.addObject("urlBase",AWebController.URL_BASE);
		return view;
	}
}
