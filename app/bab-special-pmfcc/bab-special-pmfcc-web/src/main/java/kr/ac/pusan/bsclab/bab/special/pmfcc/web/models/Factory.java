package kr.ac.pusan.bsclab.bab.special.pmfcc.web.models;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Factory {
	
	@Id
	@GeneratedValue
	@Column(name = "factory_id", nullable=false)
	private long factoryId;
	
	@Column(name = "factory_code", length = 12, nullable=false)
	private String factoryCode;
	
	@Column(name = "factory_name", length = 50, nullable=false)
	private String  factoryName;
	
	public long getFactoryId() {
		return factoryId;
	}
	
	public void setFactoryId(long factoryId) {
		this.factoryId = factoryId;
	}
	
	public String getFactoryCode() {
		return factoryCode;
	}
	
	public void setFactoryCode(String factoryCode) {
		this.factoryCode = factoryCode;
	}
	
	
	public String getFactoryName() {
		return factoryName;
	}
	
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
}
