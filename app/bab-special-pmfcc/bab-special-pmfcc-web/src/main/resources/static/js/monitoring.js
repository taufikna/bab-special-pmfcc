function initCheckbox() {
	$('.ui.checkbox').checkbox();
}

/*
 * Global variables regarding chart
 */
var DATA_CHART, GRAPH, VIS, IS_REFRESH_RUN=false, CNT=0;
/*
 * generate chart http://dygraphs.com/
 */
function renderChart() {
	
	var factory_id = $('#factory_select').val();
    var mc_id = $('#machines_select').val();
    var data_period = $('#data_period').val();
    
    CNT++;
    if(CNT==40) location.reload();
    $.ajax({
        url: 'monitoring_rest/get_results_by_hour',
        type: 'POST',
        dataType : 'json',
		contentType : "application/json",
        data: JSON.stringify({
            "factory_id":factory_id,
            "mc_id": mc_id,
            "hour":data_period,
            "cnt": CNT 
        }),
        success: function(log) {  
            DATA_CHART = log;  
            
            for(var i=0; i<DATA_CHART['data'].length; i++) {
                DATA_CHART['data'][i][0] = new Date(DATA_CHART['data'][i][0]);
                
                for(var j=1; j<DATA_CHART['data'][i].length; j++) {
                	DATA_CHART['data'][i][j] = parseFloat(DATA_CHART['data'][i][j]);
                }
            }

            VIS = [];
            for(var i=0; i<DATA_CHART['column_idx'].length; i++) {
                if($('[name=D'+DATA_CHART['column_idx'][i]+']').is(":checked"))
                    VIS.push(true);
                else
                VIS.push(false);
            }

            if(DATA_CHART['data'].length < 1) {
                alert('No data.');
                return;
            }
           
            GRAPH = new Dygraph(document.getElementById("chart"),
            DATA_CHART['data'],
            {
                labels: DATA_CHART['label'],
                legend: 'always',
                visibility: VIS
            });

            $('#timer').html(DATA_CHART['last_time'][0]);

            if(!IS_REFRESH_RUN)runRefresh();
        },
        error: function(x, e) {
            console.log(e);
        }
    });
}

/*
 * run every 1 seconds
 * still need to improve and optimize the approach
 */
function runRefresh() {
    IS_REFRESH_RUN=true;
    window.intervalId = setInterval(function() {
        renderChart();
    }, 1000);
}

/*
 * On Change Action for parameters Checkbox
 */
$(document).on("change", '.checkparameter', function() { 
    VIS = [];
    for(var i=0; i<DATA_CHART['column_idx'].length; i++) {
        if($('[name=D'+DATA_CHART['column_idx'][i]+']').is(":checked"))
            VIS.push(true);
        else
            VIS.push(false);
    }

    GRAPH.updateOptions( { visibility: VIS } );
});

/*
 * shifting event click
 */
$('.shifthour').click(function(){
    var dt = $('#end_date_field').val()+':00';
    dt = dt.replace("-", "/");
    dt = new Date(dt);

    if($(this).attr('data') == 'forward')
        dt.setHours(dt.getHours()+1);
    else
        dt.setHours(dt.getHours()-1);
        
    var newDate = dt.getFullYear()+'-'+(dt.getMonth() + 1)+'-'+dt.getDate()+' '+dt.getHours()+':'+pad(dt.getMinutes().toString(), 2);
    $('#end_date_field').val(newDate);

    renderChart();
});

/*
 * select all checkbox event click
 */
$('.selectall').click(function(){
    if($(this).attr('data') == 'select') {
        for(var i=0; i<DATA_CHART['column_idx'].length - 1; i++) {
            $('[name=D'+DATA_CHART['column_idx'][i]+']').prop('checked', true);
        }   
        $('[name=D'+DATA_CHART['column_idx'][DATA_CHART['column_idx'].length - 1]+']').prop('checked', true).trigger('change'); 
    }
    else {
        for(var i=0; i<DATA_CHART['column_idx'].length - 1; i++) {
            $('[name=D'+DATA_CHART['column_idx'][i]+']').prop('checked', false);
        }   
        $('[name=D'+DATA_CHART['column_idx'][DATA_CHART['column_idx'].length - 1]+']').prop('checked', false).trigger('change');  
    }
});

function selectFieldChanges() {
	
	$.ajax({
		url : 'monitoring_rest/get_factories',
		type : 'POST',
		dataType : 'json',
		contentType : "application/json",
		success : function(res) {
			for (var i=0; i<res.length; i++) {
			    var opt = "<option value='" + res[i].id
				+ "'>" + res[i].name + "</option>";
			    
			    $("#factory_select").append(opt);
			}
			
			$('#factory_select').trigger('change');
		},
		error : function(x, e) {
			console.log(e);
		}
	});
	
	$('#factory_select').on(
			'change',
			function() {
				var factory_id = this.value;
				$.ajax({
					url : 'monitoring_rest/get_machines',
					dataType : 'json',
					contentType : "application/json",
					type : 'POST',
					data : JSON.stringify({
						"id" : factory_id
					}),
					success : function(res) {
						if (res.length > 0) {
							$('#machines_select').empty();
							for (var i=0; i<res.length; i++) {
								var opt = "<option value='" + res[i].id
										+ "'>" + res[i].name + "</option>";
								if (res[i].id == machineIdPrev) {
									opt = "<option value='" + res[i].id
											+ "' selected>" + res[i].name
											+ "</option>";
									machineIdPrev = '';
								}
								$("#machines_select").append(opt);

								$('#machines_select').trigger('change');
							}
							renderChart();
						} else {
							$('#machines_select').empty();
							$("#machines_select")
									.append("<option>설비명</option>");
						}
					},
					error : function(x, e) {
						console.log(e);
					}
				});
			});

	/*
	 * #machines_select On Change Action for Machine Combo Box To Show Machine
	 * parameters in the right side of chart
	 */
	$('#machines_select').on('change', function() {
		var mc_id = this.value;
		$.ajax({
			url : 'monitoring_rest/get_machine_parameters',
			type : 'POST',
			contentType : "application/json",
			data : JSON.stringify({
				"id" : mc_id
			}),
			success : function(response) {
				$('#cbx_machine_parameters').empty();
				$('#cbx_machine_parameters').html(response);
				$('.ui.checkbox').checkbox();
			},
			error : function(x, e) {
				console.log(e);
			}
		});
	});

}

// JQuery entry point
$(document).ready(function() {

	// import navigation.js
	initSidebar();
	initDropdown();

	initCheckbox();

	// Simulate loading
	setTimeout(function() {
		$('.ui.active.inverted.dimmer').remove('.active');
	}, 3000);

	selectFieldChanges();
	
	$('.runquery').on('click', function() {
        renderChart();
    })

});