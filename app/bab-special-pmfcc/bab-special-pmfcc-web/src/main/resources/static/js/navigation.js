let sidebarToggle;
    
function initSidebar() {
  $('.ui.sidebar').sidebar();
  sidebarToggle = true;
  adjustTopNavbar();
}

function initDropdown() {
  $('.ui.dropdown').dropdown();
}

function adjustTopNavbar() {
  setTimeout(function() {
    const sidebarLogoItemHeight = parseInt($('.ui.vertical.sidebar.menu a.logo.item').css('height').replace(/px/, ''));
    $('.ui.top.fixed.menu').css('height', sidebarLogoItemHeight + 'px');
    $('.pusher').css('padding-top', sidebarLogoItemHeight);
  }, 10);
}

function toggleTopNavbar() {
  if(sidebarToggle === true) {
    $('.ui.sidebar').css({ width: 0, opacity: 0 });
    $('.top.fixed.menu').css({ 'padding-left': 0 });
    $('.ui.footer.basic.segment').css({ 'padding-left': 0 });
    $('.pusher').css({ 'padding-left': 0 });
    sidebarToggle = false;
    console.log(sidebarToggle);
  } else {
    $('.ui.sidebar').css({ width: '260px', opacity: 1 });
    $('.top.fixed.menu').css({ 'padding-left': '260px' });
    $('.ui.footer.basic.segment').css({ 'padding-left': '260px' });
    $('.pusher').css({ 'padding-left': '260px' });
    sidebarToggle = true;
    console.log(sidebarToggle);
  }
}